﻿namespace PizzaCompany.OrderForm
{
    partial class OrderForms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProductFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.CategoryFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSizeS = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listView1 = new System.Windows.Forms.ListView();
            this.kryptonPalette1 = new ComponentFactory.Krypton.Toolkit.KryptonPalette(this.components);
            this.LQty = new System.Windows.Forms.Label();
            this.LTotal = new System.Windows.Forms.Label();
            this.kryptonGroup2 = new ComponentFactory.Krypton.Toolkit.KryptonGroup();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.lOrder = new System.Windows.Forms.Label();
            this.btnClear = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnPay = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.flowLayoutPanel1 = new ComponentFactory.Krypton.Toolkit.KryptonGroup();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSizeL = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnSizeM = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label4 = new System.Windows.Forms.Label();
            this.kryptonGroup1 = new ComponentFactory.Krypton.Toolkit.KryptonGroup();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnThinCrust = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnHandTossed = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnCrustHotDog = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label9 = new System.Windows.Forms.Label();
            this.btnCrustCheese = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonGroup3 = new ComponentFactory.Krypton.Toolkit.KryptonGroup();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.btnTPaneer = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnTTomato = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label15 = new System.Windows.Forms.Label();
            this.btnTBBQ = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label16 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup2.Panel)).BeginInit();
            this.kryptonGroup2.Panel.SuspendLayout();
            this.kryptonGroup2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flowLayoutPanel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.flowLayoutPanel1.Panel)).BeginInit();
            this.flowLayoutPanel1.Panel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup1.Panel)).BeginInit();
            this.kryptonGroup1.Panel.SuspendLayout();
            this.kryptonGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup3.Panel)).BeginInit();
            this.kryptonGroup3.Panel.SuspendLayout();
            this.kryptonGroup3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // ProductFlow
            // 
            this.ProductFlow.Location = new System.Drawing.Point(301, 110);
            this.ProductFlow.Name = "ProductFlow";
            this.ProductFlow.Size = new System.Drawing.Size(524, 375);
            this.ProductFlow.TabIndex = 0;
            // 
            // CategoryFlow
            // 
            this.CategoryFlow.Location = new System.Drawing.Point(164, 110);
            this.CategoryFlow.Name = "CategoryFlow";
            this.CategoryFlow.Size = new System.Drawing.Size(130, 375);
            this.CategoryFlow.TabIndex = 1;
            this.CategoryFlow.Paint += new System.Windows.Forms.PaintEventHandler(this.CategoryFlow_Paint);
            // 
            // btnSizeS
            // 
            this.btnSizeS.Location = new System.Drawing.Point(1, 1);
            this.btnSizeS.Margin = new System.Windows.Forms.Padding(1);
            this.btnSizeS.Name = "btnSizeS";
            this.btnSizeS.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeS.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeS.OverrideDefault.Back.ColorAngle = 45F;
            this.btnSizeS.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnSizeS.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeS.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeS.OverrideDefault.Border.ColorAngle = 45F;
            this.btnSizeS.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeS.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeS.OverrideDefault.Border.Rounding = 20;
            this.btnSizeS.OverrideDefault.Border.Width = 1;
            this.btnSizeS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnSizeS.Size = new System.Drawing.Size(120, 60);
            this.btnSizeS.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeS.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeS.StateCommon.Back.ColorAngle = 45F;
            this.btnSizeS.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnSizeS.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnSizeS.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnSizeS.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeS.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeS.StateCommon.Border.ColorAngle = 45F;
            this.btnSizeS.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeS.StateCommon.Border.Rounding = 20;
            this.btnSizeS.StateCommon.Border.Width = 1;
            this.btnSizeS.StateCommon.Content.Padding = new System.Windows.Forms.Padding(3, -1, -1, -1);
            this.btnSizeS.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnSizeS.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSizeS.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StatePressed.Back.ColorAngle = 135F;
            this.btnSizeS.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StatePressed.Border.ColorAngle = 135F;
            this.btnSizeS.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeS.StatePressed.Border.Rounding = 20;
            this.btnSizeS.StatePressed.Border.Width = 1;
            this.btnSizeS.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StateTracking.Back.ColorAngle = 45F;
            this.btnSizeS.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeS.StateTracking.Border.ColorAngle = 45F;
            this.btnSizeS.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeS.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeS.StateTracking.Border.Rounding = 20;
            this.btnSizeS.StateTracking.Border.Width = 1;
            this.btnSizeS.TabIndex = 113;
            this.btnSizeS.Values.Text = "";
            this.btnSizeS.Click += new System.EventHandler(this.btnSizeS_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tw Cen MT", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.label1.Location = new System.Drawing.Point(160, 524);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tw Cen MT", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.label2.Location = new System.Drawing.Point(160, 596);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Crust";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tw Cen MT", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.label3.Location = new System.Drawing.Point(160, 667);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Topping";
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(841, 110);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(366, 430);
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // kryptonPalette1
            // 
            this.kryptonPalette1.ButtonSpecs.FormClose.Image = global::PizzaCompany.Properties.Resources.Red;
            this.kryptonPalette1.ButtonSpecs.FormClose.ImageStates.ImageNormal = global::PizzaCompany.Properties.Resources.Red;
            this.kryptonPalette1.ButtonSpecs.FormClose.ImageStates.ImagePressed = global::PizzaCompany.Properties.Resources.Red;
            this.kryptonPalette1.ButtonSpecs.FormClose.ImageStates.ImageTracking = global::PizzaCompany.Properties.Resources.Red;
            this.kryptonPalette1.ButtonSpecs.FormMax.Image = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormMax.ImageStates.ImageNormal = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormMax.ImageStates.ImagePressed = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormMax.ImageStates.ImageTracking = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormMin.Image = global::PizzaCompany.Properties.Resources.Green;
            this.kryptonPalette1.ButtonSpecs.FormMin.ImageStates.ImageNormal = global::PizzaCompany.Properties.Resources.Green;
            this.kryptonPalette1.ButtonSpecs.FormMin.ImageStates.ImagePressed = global::PizzaCompany.Properties.Resources.Green;
            this.kryptonPalette1.ButtonSpecs.FormMin.ImageStates.ImageTracking = global::PizzaCompany.Properties.Resources.Green;
            this.kryptonPalette1.ButtonSpecs.FormRestore.Image = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormRestore.ImageStates.ImageNormal = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormRestore.ImageStates.ImagePressed = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonSpecs.FormRestore.ImageStates.ImageTracking = global::PizzaCompany.Properties.Resources.Yellow;
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateNormal.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateNormal.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateNormal.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateNormal.Border.Width = 0;
            this.kryptonPalette1.ButtonStyles.ButtonForm.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StatePressed.Border.Width = 0;
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonPalette1.ButtonStyles.ButtonForm.StateTracking.Border.Width = 0;
            this.kryptonPalette1.FormStyles.FormMain.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.FormStyles.FormMain.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.FormStyles.FormMain.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.kryptonPalette1.FormStyles.FormMain.StateCommon.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.None;
            this.kryptonPalette1.FormStyles.FormMain.StateCommon.Border.Rounding = 12;
            this.kryptonPalette1.HeaderStyles.HeaderForm.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.HeaderStyles.HeaderForm.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.kryptonPalette1.HeaderStyles.HeaderForm.StateCommon.ButtonEdgeInset = 10;
            this.kryptonPalette1.HeaderStyles.HeaderForm.StateCommon.Content.Padding = new System.Windows.Forms.Padding(10, -1, -1, -1);
            // 
            // LQty
            // 
            this.LQty.AutoSize = true;
            this.LQty.BackColor = System.Drawing.Color.Transparent;
            this.LQty.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LQty.Font = new System.Drawing.Font("Tw Cen MT", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LQty.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.LQty.Location = new System.Drawing.Point(836, 556);
            this.LQty.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LQty.Name = "LQty";
            this.LQty.Size = new System.Drawing.Size(55, 28);
            this.LQty.TabIndex = 111;
            this.LQty.Text = "Qty:";
            // 
            // LTotal
            // 
            this.LTotal.AutoSize = true;
            this.LTotal.BackColor = System.Drawing.Color.Transparent;
            this.LTotal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LTotal.Font = new System.Drawing.Font("Tw Cen MT", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.LTotal.Location = new System.Drawing.Point(1029, 556);
            this.LTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LTotal.Name = "LTotal";
            this.LTotal.Size = new System.Drawing.Size(68, 28);
            this.LTotal.TabIndex = 112;
            this.LTotal.Text = "Total:";
            // 
            // kryptonGroup2
            // 
            this.kryptonGroup2.Location = new System.Drawing.Point(0, 0);
            this.kryptonGroup2.Name = "kryptonGroup2";
            // 
            // kryptonGroup2.Panel
            // 
            this.kryptonGroup2.Panel.Controls.Add(this.pictureBox4);
            this.kryptonGroup2.Panel.Controls.Add(this.pictureBox7);
            this.kryptonGroup2.Panel.Controls.Add(this.lOrder);
            this.kryptonGroup2.Size = new System.Drawing.Size(100, 730);
            this.kryptonGroup2.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.kryptonGroup2.TabIndex = 113;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.pictureBox4.Image = global::PizzaCompany.Properties.Resources.food__3_3;
            this.pictureBox4.Location = new System.Drawing.Point(34, 141);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(32, 32);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 24;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.pictureBox7.Image = global::PizzaCompany.Properties.Resources.logo;
            this.pictureBox7.Location = new System.Drawing.Point(10, 35);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(79, 50);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 20;
            this.pictureBox7.TabStop = false;
            // 
            // lOrder
            // 
            this.lOrder.AutoSize = true;
            this.lOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.lOrder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lOrder.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lOrder.ForeColor = System.Drawing.Color.White;
            this.lOrder.Location = new System.Drawing.Point(26, 178);
            this.lOrder.Name = "lOrder";
            this.lOrder.Size = new System.Drawing.Size(48, 19);
            this.lOrder.TabIndex = 9;
            this.lOrder.Text = "Order";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(1082, 661);
            this.btnClear.Name = "btnClear";
            this.btnClear.OverrideDefault.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.btnClear.OverrideDefault.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.OverrideDefault.Back.ColorAngle = 45F;
            this.btnClear.OverrideDefault.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.btnClear.OverrideDefault.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.OverrideDefault.Border.ColorAngle = 45F;
            this.btnClear.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnClear.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnClear.OverrideDefault.Border.Rounding = 20;
            this.btnClear.OverrideDefault.Border.Width = 1;
            this.btnClear.OverrideDefault.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnClear.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnClear.Size = new System.Drawing.Size(125, 49);
            this.btnClear.StateCommon.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.btnClear.StateCommon.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.StateCommon.Back.ColorAngle = 45F;
            this.btnClear.StateCommon.Back.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnClear.StateCommon.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.btnClear.StateCommon.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.StateCommon.Border.ColorAngle = 45F;
            this.btnClear.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnClear.StateCommon.Border.Rounding = 30;
            this.btnClear.StateCommon.Border.Width = 1;
            this.btnClear.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnClear.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.btnClear.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.btnClear.StatePressed.Back.ColorAngle = 135F;
            this.btnClear.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.btnClear.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.btnClear.StatePressed.Border.ColorAngle = 135F;
            this.btnClear.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnClear.StatePressed.Border.Rounding = 20;
            this.btnClear.StatePressed.Border.Width = 1;
            this.btnClear.StatePressed.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnClear.StatePressed.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(101)))), ((int)(((byte)(11)))), ((int)(((byte)(12)))));
            this.btnClear.StateTracking.Back.ColorAngle = 45F;
            this.btnClear.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(21)))), ((int)(((byte)(23)))));
            this.btnClear.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.btnClear.StateTracking.Border.ColorAngle = 45F;
            this.btnClear.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnClear.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnClear.StateTracking.Border.Rounding = 20;
            this.btnClear.StateTracking.Border.Width = 1;
            this.btnClear.StateTracking.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnClear.StateTracking.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.TabIndex = 109;
            this.btnClear.Values.Image = global::PizzaCompany.Properties.Resources.clear;
            this.btnClear.Values.Text = "Clear";
            // 
            // btnPay
            // 
            this.btnPay.Location = new System.Drawing.Point(1082, 603);
            this.btnPay.Name = "btnPay";
            this.btnPay.OverrideDefault.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.btnPay.OverrideDefault.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.btnPay.OverrideDefault.Back.ColorAngle = 45F;
            this.btnPay.OverrideDefault.Border.Color1 = System.Drawing.Color.Green;
            this.btnPay.OverrideDefault.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPay.OverrideDefault.Border.ColorAngle = 45F;
            this.btnPay.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnPay.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnPay.OverrideDefault.Border.Rounding = 20;
            this.btnPay.OverrideDefault.Border.Width = 1;
            this.btnPay.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnPay.Size = new System.Drawing.Size(125, 49);
            this.btnPay.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnPay.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnPay.StateCommon.Back.ColorAngle = 45F;
            this.btnPay.StateCommon.Back.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnPay.StateCommon.Border.Color1 = System.Drawing.Color.Green;
            this.btnPay.StateCommon.Border.Color2 = System.Drawing.Color.Green;
            this.btnPay.StateCommon.Border.ColorAngle = 45F;
            this.btnPay.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnPay.StateCommon.Border.Rounding = 30;
            this.btnPay.StateCommon.Border.Width = 1;
            this.btnPay.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.btnPay.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.StatePressed.Back.Color1 = System.Drawing.Color.Green;
            this.btnPay.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPay.StatePressed.Back.ColorAngle = 135F;
            this.btnPay.StatePressed.Border.Color1 = System.Drawing.Color.Green;
            this.btnPay.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPay.StatePressed.Border.ColorAngle = 135F;
            this.btnPay.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnPay.StatePressed.Border.Rounding = 20;
            this.btnPay.StatePressed.Border.Width = 1;
            this.btnPay.StatePressed.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnPay.StatePressed.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.StateTracking.Back.Color1 = System.Drawing.Color.Green;
            this.btnPay.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPay.StateTracking.Back.ColorAngle = 45F;
            this.btnPay.StateTracking.Border.Color1 = System.Drawing.Color.Green;
            this.btnPay.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnPay.StateTracking.Border.ColorAngle = 45F;
            this.btnPay.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnPay.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnPay.StateTracking.Border.Rounding = 20;
            this.btnPay.StateTracking.Border.Width = 1;
            this.btnPay.StateTracking.Content.ShortText.Color1 = System.Drawing.Color.White;
            this.btnPay.StateTracking.Content.ShortText.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.TabIndex = 110;
            this.btnPay.Values.ImageStates.ImageCheckedNormal = null;
            this.btnPay.Values.ImageStates.ImageCheckedPressed = null;
            this.btnPay.Values.ImageStates.ImageCheckedTracking = null;
            this.btnPay.Values.ImageStates.ImageDisabled = global::PizzaCompany.Properties.Resources.wallet__3_;
            this.btnPay.Values.ImageStates.ImageNormal = global::PizzaCompany.Properties.Resources.wallet__3_1;
            this.btnPay.Values.ImageStates.ImagePressed = global::PizzaCompany.Properties.Resources.wallet__2_;
            this.btnPay.Values.ImageStates.ImageTracking = global::PizzaCompany.Properties.Resources.wallet__2_1;
            this.btnPay.Values.Text = "Payment";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonForm;
            this.flowLayoutPanel1.GroupBorderStyle = ComponentFactory.Krypton.Toolkit.PaletteBorderStyle.ButtonForm;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(236, 500);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // flowLayoutPanel1.Panel
            // 
            this.flowLayoutPanel1.Panel.Controls.Add(this.label6);
            this.flowLayoutPanel1.Panel.Controls.Add(this.label5);
            this.flowLayoutPanel1.Panel.Controls.Add(this.btnSizeL);
            this.flowLayoutPanel1.Panel.Controls.Add(this.btnSizeM);
            this.flowLayoutPanel1.Panel.Controls.Add(this.label4);
            this.flowLayoutPanel1.Panel.Controls.Add(this.btnSizeS);
            this.flowLayoutPanel1.Panel.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Panel_Paint);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(411, 69);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label6.Location = new System.Drawing.Point(332, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 19);
            this.label6.TabIndex = 117;
            this.label6.Text = "Size L";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label5.Location = new System.Drawing.Point(194, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 116;
            this.label5.Text = "Size M";
            // 
            // btnSizeL
            // 
            this.btnSizeL.Location = new System.Drawing.Point(270, 1);
            this.btnSizeL.Margin = new System.Windows.Forms.Padding(1);
            this.btnSizeL.Name = "btnSizeL";
            this.btnSizeL.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeL.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeL.OverrideDefault.Back.ColorAngle = 45F;
            this.btnSizeL.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnSizeL.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeL.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeL.OverrideDefault.Border.ColorAngle = 45F;
            this.btnSizeL.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeL.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeL.OverrideDefault.Border.Rounding = 20;
            this.btnSizeL.OverrideDefault.Border.Width = 1;
            this.btnSizeL.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnSizeL.Size = new System.Drawing.Size(120, 60);
            this.btnSizeL.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeL.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeL.StateCommon.Back.ColorAngle = 45F;
            this.btnSizeL.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.L;
            this.btnSizeL.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnSizeL.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnSizeL.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeL.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeL.StateCommon.Border.ColorAngle = 45F;
            this.btnSizeL.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeL.StateCommon.Border.Rounding = 20;
            this.btnSizeL.StateCommon.Border.Width = 1;
            this.btnSizeL.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnSizeL.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSizeL.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StatePressed.Back.ColorAngle = 135F;
            this.btnSizeL.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StatePressed.Border.ColorAngle = 135F;
            this.btnSizeL.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeL.StatePressed.Border.Rounding = 20;
            this.btnSizeL.StatePressed.Border.Width = 1;
            this.btnSizeL.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StateTracking.Back.ColorAngle = 45F;
            this.btnSizeL.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeL.StateTracking.Border.ColorAngle = 45F;
            this.btnSizeL.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeL.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeL.StateTracking.Border.Rounding = 20;
            this.btnSizeL.StateTracking.Border.Width = 1;
            this.btnSizeL.TabIndex = 114;
            this.btnSizeL.Values.Text = "";
            // 
            // btnSizeM
            // 
            this.btnSizeM.Location = new System.Drawing.Point(135, 1);
            this.btnSizeM.Margin = new System.Windows.Forms.Padding(1);
            this.btnSizeM.Name = "btnSizeM";
            this.btnSizeM.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeM.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeM.OverrideDefault.Back.ColorAngle = 45F;
            this.btnSizeM.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnSizeM.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeM.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeM.OverrideDefault.Border.ColorAngle = 45F;
            this.btnSizeM.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeM.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeM.OverrideDefault.Border.Rounding = 20;
            this.btnSizeM.OverrideDefault.Border.Width = 1;
            this.btnSizeM.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnSizeM.Size = new System.Drawing.Size(120, 60);
            this.btnSizeM.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnSizeM.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnSizeM.StateCommon.Back.ColorAngle = 45F;
            this.btnSizeM.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.m1;
            this.btnSizeM.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnSizeM.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnSizeM.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnSizeM.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnSizeM.StateCommon.Border.ColorAngle = 45F;
            this.btnSizeM.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeM.StateCommon.Border.Rounding = 20;
            this.btnSizeM.StateCommon.Border.Width = 1;
            this.btnSizeM.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnSizeM.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSizeM.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StatePressed.Back.ColorAngle = 135F;
            this.btnSizeM.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StatePressed.Border.ColorAngle = 135F;
            this.btnSizeM.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeM.StatePressed.Border.Rounding = 20;
            this.btnSizeM.StatePressed.Border.Width = 1;
            this.btnSizeM.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StateTracking.Back.ColorAngle = 45F;
            this.btnSizeM.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnSizeM.StateTracking.Border.ColorAngle = 45F;
            this.btnSizeM.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnSizeM.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnSizeM.StateTracking.Border.Rounding = 20;
            this.btnSizeM.StateTracking.Border.Width = 1;
            this.btnSizeM.TabIndex = 115;
            this.btnSizeM.Values.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label4.Location = new System.Drawing.Point(54, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 19);
            this.label4.TabIndex = 114;
            this.label4.Text = "Size S";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // kryptonGroup1
            // 
            this.kryptonGroup1.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonForm;
            this.kryptonGroup1.GroupBorderStyle = ComponentFactory.Krypton.Toolkit.PaletteBorderStyle.ButtonForm;
            this.kryptonGroup1.Location = new System.Drawing.Point(236, 575);
            this.kryptonGroup1.Name = "kryptonGroup1";
            // 
            // kryptonGroup1.Panel
            // 
            this.kryptonGroup1.Panel.Controls.Add(this.label11);
            this.kryptonGroup1.Panel.Controls.Add(this.label12);
            this.kryptonGroup1.Panel.Controls.Add(this.btnThinCrust);
            this.kryptonGroup1.Panel.Controls.Add(this.label10);
            this.kryptonGroup1.Panel.Controls.Add(this.label7);
            this.kryptonGroup1.Panel.Controls.Add(this.label8);
            this.kryptonGroup1.Panel.Controls.Add(this.btnHandTossed);
            this.kryptonGroup1.Panel.Controls.Add(this.btnCrustHotDog);
            this.kryptonGroup1.Panel.Controls.Add(this.label9);
            this.kryptonGroup1.Panel.Controls.Add(this.btnCrustCheese);
            this.kryptonGroup1.Size = new System.Drawing.Size(539, 69);
            this.kryptonGroup1.TabIndex = 114;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label11.Location = new System.Drawing.Point(55, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 19);
            this.label11.TabIndex = 122;
            this.label11.Text = "Crust";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label12.Location = new System.Drawing.Point(55, 12);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 19);
            this.label12.TabIndex = 120;
            this.label12.Text = "Thin";
            // 
            // btnThinCrust
            // 
            this.btnThinCrust.Location = new System.Drawing.Point(1, 1);
            this.btnThinCrust.Margin = new System.Windows.Forms.Padding(1);
            this.btnThinCrust.Name = "btnThinCrust";
            this.btnThinCrust.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnThinCrust.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnThinCrust.OverrideDefault.Back.ColorAngle = 45F;
            this.btnThinCrust.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnThinCrust.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnThinCrust.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnThinCrust.OverrideDefault.Border.ColorAngle = 45F;
            this.btnThinCrust.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnThinCrust.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnThinCrust.OverrideDefault.Border.Rounding = 20;
            this.btnThinCrust.OverrideDefault.Border.Width = 1;
            this.btnThinCrust.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnThinCrust.Size = new System.Drawing.Size(120, 60);
            this.btnThinCrust.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnThinCrust.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnThinCrust.StateCommon.Back.ColorAngle = 45F;
            this.btnThinCrust.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.thin;
            this.btnThinCrust.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnThinCrust.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnThinCrust.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnThinCrust.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnThinCrust.StateCommon.Border.ColorAngle = 45F;
            this.btnThinCrust.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnThinCrust.StateCommon.Border.Rounding = 20;
            this.btnThinCrust.StateCommon.Border.Width = 1;
            this.btnThinCrust.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnThinCrust.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThinCrust.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StatePressed.Back.ColorAngle = 135F;
            this.btnThinCrust.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StatePressed.Border.ColorAngle = 135F;
            this.btnThinCrust.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnThinCrust.StatePressed.Border.Rounding = 20;
            this.btnThinCrust.StatePressed.Border.Width = 1;
            this.btnThinCrust.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StateTracking.Back.ColorAngle = 45F;
            this.btnThinCrust.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnThinCrust.StateTracking.Border.ColorAngle = 45F;
            this.btnThinCrust.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnThinCrust.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnThinCrust.StateTracking.Border.Rounding = 20;
            this.btnThinCrust.StateTracking.Border.Width = 1;
            this.btnThinCrust.TabIndex = 119;
            this.btnThinCrust.Values.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label10.Location = new System.Drawing.Point(185, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(57, 19);
            this.label10.TabIndex = 119;
            this.label10.Text = "Tossed";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label7.Location = new System.Drawing.Point(185, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 19);
            this.label7.TabIndex = 117;
            this.label7.Text = "Hand";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label8.Location = new System.Drawing.Point(455, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 19);
            this.label8.TabIndex = 116;
            this.label8.Text = "HotDog";
            // 
            // btnHandTossed
            // 
            this.btnHandTossed.Location = new System.Drawing.Point(135, 1);
            this.btnHandTossed.Margin = new System.Windows.Forms.Padding(1);
            this.btnHandTossed.Name = "btnHandTossed";
            this.btnHandTossed.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnHandTossed.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnHandTossed.OverrideDefault.Back.ColorAngle = 45F;
            this.btnHandTossed.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnHandTossed.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnHandTossed.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnHandTossed.OverrideDefault.Border.ColorAngle = 45F;
            this.btnHandTossed.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnHandTossed.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnHandTossed.OverrideDefault.Border.Rounding = 20;
            this.btnHandTossed.OverrideDefault.Border.Width = 1;
            this.btnHandTossed.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnHandTossed.Size = new System.Drawing.Size(120, 60);
            this.btnHandTossed.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnHandTossed.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnHandTossed.StateCommon.Back.ColorAngle = 45F;
            this.btnHandTossed.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.hand;
            this.btnHandTossed.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnHandTossed.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnHandTossed.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnHandTossed.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnHandTossed.StateCommon.Border.ColorAngle = 45F;
            this.btnHandTossed.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnHandTossed.StateCommon.Border.Rounding = 20;
            this.btnHandTossed.StateCommon.Border.Width = 1;
            this.btnHandTossed.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnHandTossed.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHandTossed.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StatePressed.Back.ColorAngle = 135F;
            this.btnHandTossed.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StatePressed.Border.ColorAngle = 135F;
            this.btnHandTossed.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnHandTossed.StatePressed.Border.Rounding = 20;
            this.btnHandTossed.StatePressed.Border.Width = 1;
            this.btnHandTossed.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StateTracking.Back.ColorAngle = 45F;
            this.btnHandTossed.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnHandTossed.StateTracking.Border.ColorAngle = 45F;
            this.btnHandTossed.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnHandTossed.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnHandTossed.StateTracking.Border.Rounding = 20;
            this.btnHandTossed.StateTracking.Border.Width = 1;
            this.btnHandTossed.TabIndex = 118;
            this.btnHandTossed.Values.Text = "";
            // 
            // btnCrustHotDog
            // 
            this.btnCrustHotDog.Location = new System.Drawing.Point(404, 1);
            this.btnCrustHotDog.Margin = new System.Windows.Forms.Padding(1);
            this.btnCrustHotDog.Name = "btnCrustHotDog";
            this.btnCrustHotDog.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnCrustHotDog.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnCrustHotDog.OverrideDefault.Back.ColorAngle = 45F;
            this.btnCrustHotDog.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnCrustHotDog.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnCrustHotDog.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnCrustHotDog.OverrideDefault.Border.ColorAngle = 45F;
            this.btnCrustHotDog.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustHotDog.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCrustHotDog.OverrideDefault.Border.Rounding = 20;
            this.btnCrustHotDog.OverrideDefault.Border.Width = 1;
            this.btnCrustHotDog.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnCrustHotDog.Size = new System.Drawing.Size(120, 60);
            this.btnCrustHotDog.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnCrustHotDog.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnCrustHotDog.StateCommon.Back.ColorAngle = 45F;
            this.btnCrustHotDog.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.sausage;
            this.btnCrustHotDog.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnCrustHotDog.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnCrustHotDog.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnCrustHotDog.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnCrustHotDog.StateCommon.Border.ColorAngle = 45F;
            this.btnCrustHotDog.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustHotDog.StateCommon.Border.Rounding = 20;
            this.btnCrustHotDog.StateCommon.Border.Width = 1;
            this.btnCrustHotDog.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnCrustHotDog.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrustHotDog.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StatePressed.Back.ColorAngle = 135F;
            this.btnCrustHotDog.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StatePressed.Border.ColorAngle = 135F;
            this.btnCrustHotDog.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustHotDog.StatePressed.Border.Rounding = 20;
            this.btnCrustHotDog.StatePressed.Border.Width = 1;
            this.btnCrustHotDog.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StateTracking.Back.ColorAngle = 45F;
            this.btnCrustHotDog.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustHotDog.StateTracking.Border.ColorAngle = 45F;
            this.btnCrustHotDog.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustHotDog.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCrustHotDog.StateTracking.Border.Rounding = 20;
            this.btnCrustHotDog.StateTracking.Border.Width = 1;
            this.btnCrustHotDog.TabIndex = 117;
            this.btnCrustHotDog.Values.Text = "";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label9.Location = new System.Drawing.Point(318, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 19);
            this.label9.TabIndex = 114;
            this.label9.Text = "Cheese";
            // 
            // btnCrustCheese
            // 
            this.btnCrustCheese.Location = new System.Drawing.Point(270, 1);
            this.btnCrustCheese.Margin = new System.Windows.Forms.Padding(1);
            this.btnCrustCheese.Name = "btnCrustCheese";
            this.btnCrustCheese.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnCrustCheese.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnCrustCheese.OverrideDefault.Back.ColorAngle = 45F;
            this.btnCrustCheese.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnCrustCheese.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnCrustCheese.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnCrustCheese.OverrideDefault.Border.ColorAngle = 45F;
            this.btnCrustCheese.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustCheese.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCrustCheese.OverrideDefault.Border.Rounding = 20;
            this.btnCrustCheese.OverrideDefault.Border.Width = 1;
            this.btnCrustCheese.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnCrustCheese.Size = new System.Drawing.Size(120, 60);
            this.btnCrustCheese.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnCrustCheese.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnCrustCheese.StateCommon.Back.ColorAngle = 45F;
            this.btnCrustCheese.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.cheese1;
            this.btnCrustCheese.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnCrustCheese.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnCrustCheese.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnCrustCheese.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnCrustCheese.StateCommon.Border.ColorAngle = 45F;
            this.btnCrustCheese.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustCheese.StateCommon.Border.Rounding = 20;
            this.btnCrustCheese.StateCommon.Border.Width = 1;
            this.btnCrustCheese.StateCommon.Content.Padding = new System.Windows.Forms.Padding(3, -1, -1, -1);
            this.btnCrustCheese.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnCrustCheese.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrustCheese.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StatePressed.Back.ColorAngle = 135F;
            this.btnCrustCheese.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StatePressed.Border.ColorAngle = 135F;
            this.btnCrustCheese.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustCheese.StatePressed.Border.Rounding = 20;
            this.btnCrustCheese.StatePressed.Border.Width = 1;
            this.btnCrustCheese.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StateTracking.Back.ColorAngle = 45F;
            this.btnCrustCheese.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnCrustCheese.StateTracking.Border.ColorAngle = 45F;
            this.btnCrustCheese.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnCrustCheese.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnCrustCheese.StateTracking.Border.Rounding = 20;
            this.btnCrustCheese.StateTracking.Border.Width = 1;
            this.btnCrustCheese.TabIndex = 116;
            this.btnCrustCheese.Values.Text = "";
            // 
            // kryptonGroup3
            // 
            this.kryptonGroup3.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.ButtonForm;
            this.kryptonGroup3.GroupBorderStyle = ComponentFactory.Krypton.Toolkit.PaletteBorderStyle.ButtonForm;
            this.kryptonGroup3.Location = new System.Drawing.Point(239, 650);
            this.kryptonGroup3.Name = "kryptonGroup3";
            // 
            // kryptonGroup3.Panel
            // 
            this.kryptonGroup3.Panel.Controls.Add(this.label13);
            this.kryptonGroup3.Panel.Controls.Add(this.label14);
            this.kryptonGroup3.Panel.Controls.Add(this.btnTPaneer);
            this.kryptonGroup3.Panel.Controls.Add(this.btnTTomato);
            this.kryptonGroup3.Panel.Controls.Add(this.label15);
            this.kryptonGroup3.Panel.Controls.Add(this.btnTBBQ);
            this.kryptonGroup3.Size = new System.Drawing.Size(411, 70);
            this.kryptonGroup3.TabIndex = 115;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label13.Location = new System.Drawing.Point(322, 22);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 19);
            this.label13.TabIndex = 117;
            this.label13.Text = "Paneer";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label14.Location = new System.Drawing.Point(184, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 19);
            this.label14.TabIndex = 116;
            this.label14.Text = "Tomato";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // btnTPaneer
            // 
            this.btnTPaneer.Location = new System.Drawing.Point(268, 1);
            this.btnTPaneer.Margin = new System.Windows.Forms.Padding(1);
            this.btnTPaneer.Name = "btnTPaneer";
            this.btnTPaneer.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnTPaneer.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnTPaneer.OverrideDefault.Back.ColorAngle = 45F;
            this.btnTPaneer.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnTPaneer.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnTPaneer.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnTPaneer.OverrideDefault.Border.ColorAngle = 45F;
            this.btnTPaneer.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTPaneer.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTPaneer.OverrideDefault.Border.Rounding = 20;
            this.btnTPaneer.OverrideDefault.Border.Width = 1;
            this.btnTPaneer.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnTPaneer.Size = new System.Drawing.Size(120, 60);
            this.btnTPaneer.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnTPaneer.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnTPaneer.StateCommon.Back.ColorAngle = 45F;
            this.btnTPaneer.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.tc;
            this.btnTPaneer.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnTPaneer.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnTPaneer.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnTPaneer.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnTPaneer.StateCommon.Border.ColorAngle = 45F;
            this.btnTPaneer.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTPaneer.StateCommon.Border.Rounding = 20;
            this.btnTPaneer.StateCommon.Border.Width = 1;
            this.btnTPaneer.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnTPaneer.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTPaneer.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StatePressed.Back.ColorAngle = 135F;
            this.btnTPaneer.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StatePressed.Border.ColorAngle = 135F;
            this.btnTPaneer.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTPaneer.StatePressed.Border.Rounding = 20;
            this.btnTPaneer.StatePressed.Border.Width = 1;
            this.btnTPaneer.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StateTracking.Back.ColorAngle = 45F;
            this.btnTPaneer.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTPaneer.StateTracking.Border.ColorAngle = 45F;
            this.btnTPaneer.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTPaneer.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTPaneer.StateTracking.Border.Rounding = 20;
            this.btnTPaneer.StateTracking.Border.Width = 1;
            this.btnTPaneer.TabIndex = 118;
            this.btnTPaneer.Values.Text = "";
            // 
            // btnTTomato
            // 
            this.btnTTomato.Location = new System.Drawing.Point(133, 1);
            this.btnTTomato.Margin = new System.Windows.Forms.Padding(1);
            this.btnTTomato.Name = "btnTTomato";
            this.btnTTomato.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnTTomato.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnTTomato.OverrideDefault.Back.ColorAngle = 45F;
            this.btnTTomato.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnTTomato.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnTTomato.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnTTomato.OverrideDefault.Border.ColorAngle = 45F;
            this.btnTTomato.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTTomato.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTTomato.OverrideDefault.Border.Rounding = 20;
            this.btnTTomato.OverrideDefault.Border.Width = 1;
            this.btnTTomato.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnTTomato.Size = new System.Drawing.Size(120, 60);
            this.btnTTomato.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnTTomato.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnTTomato.StateCommon.Back.ColorAngle = 45F;
            this.btnTTomato.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.tt;
            this.btnTTomato.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnTTomato.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnTTomato.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnTTomato.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnTTomato.StateCommon.Border.ColorAngle = 45F;
            this.btnTTomato.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTTomato.StateCommon.Border.Rounding = 20;
            this.btnTTomato.StateCommon.Border.Width = 1;
            this.btnTTomato.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnTTomato.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTTomato.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StatePressed.Back.ColorAngle = 135F;
            this.btnTTomato.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StatePressed.Border.ColorAngle = 135F;
            this.btnTTomato.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTTomato.StatePressed.Border.Rounding = 20;
            this.btnTTomato.StatePressed.Border.Width = 1;
            this.btnTTomato.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StateTracking.Back.ColorAngle = 45F;
            this.btnTTomato.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTTomato.StateTracking.Border.ColorAngle = 45F;
            this.btnTTomato.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTTomato.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTTomato.StateTracking.Border.Rounding = 20;
            this.btnTTomato.StateTracking.Border.Width = 1;
            this.btnTTomato.TabIndex = 117;
            this.btnTTomato.Values.Text = "";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tw Cen MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(90)))), ((int)(((byte)(50)))));
            this.label15.Location = new System.Drawing.Point(56, 22);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 19);
            this.label15.TabIndex = 114;
            this.label15.Text = "BBQ";
            // 
            // btnTBBQ
            // 
            this.btnTBBQ.Location = new System.Drawing.Point(1, 1);
            this.btnTBBQ.Margin = new System.Windows.Forms.Padding(1);
            this.btnTBBQ.Name = "btnTBBQ";
            this.btnTBBQ.OverrideDefault.Back.Color1 = System.Drawing.Color.White;
            this.btnTBBQ.OverrideDefault.Back.Color2 = System.Drawing.Color.White;
            this.btnTBBQ.OverrideDefault.Back.ColorAngle = 45F;
            this.btnTBBQ.OverrideDefault.Back.Image = global::PizzaCompany.Properties.Resources.Sss1;
            this.btnTBBQ.OverrideDefault.Border.Color1 = System.Drawing.Color.White;
            this.btnTBBQ.OverrideDefault.Border.Color2 = System.Drawing.Color.White;
            this.btnTBBQ.OverrideDefault.Border.ColorAngle = 45F;
            this.btnTBBQ.OverrideDefault.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTBBQ.OverrideDefault.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTBBQ.OverrideDefault.Border.Rounding = 20;
            this.btnTBBQ.OverrideDefault.Border.Width = 1;
            this.btnTBBQ.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.ProfessionalSystem;
            this.btnTBBQ.Size = new System.Drawing.Size(120, 60);
            this.btnTBBQ.StateCommon.Back.Color1 = System.Drawing.Color.White;
            this.btnTBBQ.StateCommon.Back.Color2 = System.Drawing.Color.White;
            this.btnTBBQ.StateCommon.Back.ColorAngle = 45F;
            this.btnTBBQ.StateCommon.Back.Image = global::PizzaCompany.Properties.Resources.tb;
            this.btnTBBQ.StateCommon.Back.ImageAlign = ComponentFactory.Krypton.Toolkit.PaletteRectangleAlign.Local;
            this.btnTBBQ.StateCommon.Back.ImageStyle = ComponentFactory.Krypton.Toolkit.PaletteImageStyle.CenterLeft;
            this.btnTBBQ.StateCommon.Border.Color1 = System.Drawing.Color.White;
            this.btnTBBQ.StateCommon.Border.Color2 = System.Drawing.Color.White;
            this.btnTBBQ.StateCommon.Border.ColorAngle = 45F;
            this.btnTBBQ.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTBBQ.StateCommon.Border.Rounding = 20;
            this.btnTBBQ.StateCommon.Border.Width = 1;
            this.btnTBBQ.StateCommon.Content.Padding = new System.Windows.Forms.Padding(3, -1, -1, -1);
            this.btnTBBQ.StateCommon.Content.ShortText.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(142)))), ((int)(((byte)(254)))));
            this.btnTBBQ.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTBBQ.StatePressed.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StatePressed.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StatePressed.Back.ColorAngle = 135F;
            this.btnTBBQ.StatePressed.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StatePressed.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StatePressed.Border.ColorAngle = 135F;
            this.btnTBBQ.StatePressed.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTBBQ.StatePressed.Border.Rounding = 20;
            this.btnTBBQ.StatePressed.Border.Width = 1;
            this.btnTBBQ.StateTracking.Back.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StateTracking.Back.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StateTracking.Back.ColorAngle = 45F;
            this.btnTBBQ.StateTracking.Border.Color1 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StateTracking.Border.Color2 = System.Drawing.Color.FromArgb(((int)(((byte)(213)))), ((int)(((byte)(187)))), ((int)(((byte)(170)))));
            this.btnTBBQ.StateTracking.Border.ColorAngle = 45F;
            this.btnTBBQ.StateTracking.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btnTBBQ.StateTracking.Border.GraphicsHint = ComponentFactory.Krypton.Toolkit.PaletteGraphicsHint.AntiAlias;
            this.btnTBBQ.StateTracking.Border.Rounding = 20;
            this.btnTBBQ.StateTracking.Border.Width = 1;
            this.btnTBBQ.TabIndex = 116;
            this.btnTBBQ.Values.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label16.Font = new System.Drawing.Font("Tw Cen MT", 30F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(72)))), ((int)(((byte)(31)))), ((int)(((byte)(16)))));
            this.label16.Location = new System.Drawing.Point(212, 52);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(140, 47);
            this.label16.TabIndex = 116;
            this.label16.Text = "ORDER";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::PizzaCompany.Properties.Resources.food__6_;
            this.pictureBox3.Location = new System.Drawing.Point(167, 53);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(38, 40);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 69;
            this.pictureBox3.TabStop = false;
            // 
            // OrderForms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(252)))), ((int)(((byte)(252)))));
            this.BackgroundImage = global::PizzaCompany.Properties.Resources._11;
            this.ClientSize = new System.Drawing.Size(1264, 729);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.kryptonGroup3);
            this.Controls.Add(this.kryptonGroup1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.kryptonGroup2);
            this.Controls.Add(this.LTotal);
            this.Controls.Add(this.LQty);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CategoryFlow);
            this.Controls.Add(this.ProductFlow);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Khmer OS Battambang", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "OrderForms";
            this.Palette = this.kryptonPalette1;
            this.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Custom;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OrderForms";
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup2.Panel)).EndInit();
            this.kryptonGroup2.Panel.ResumeLayout(false);
            this.kryptonGroup2.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup2)).EndInit();
            this.kryptonGroup2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.flowLayoutPanel1.Panel)).EndInit();
            this.flowLayoutPanel1.Panel.ResumeLayout(false);
            this.flowLayoutPanel1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flowLayoutPanel1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup1.Panel)).EndInit();
            this.kryptonGroup1.Panel.ResumeLayout(false);
            this.kryptonGroup1.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup1)).EndInit();
            this.kryptonGroup1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup3.Panel)).EndInit();
            this.kryptonGroup3.Panel.ResumeLayout(false);
            this.kryptonGroup3.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonGroup3)).EndInit();
            this.kryptonGroup3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel ProductFlow;
        private System.Windows.Forms.FlowLayoutPanel CategoryFlow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView listView1;
        private ComponentFactory.Krypton.Toolkit.KryptonPalette kryptonPalette1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSizeS;
        private System.Windows.Forms.Label LQty;
        private System.Windows.Forms.Label LTotal;
        private ComponentFactory.Krypton.Toolkit.KryptonGroup kryptonGroup2;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnClear;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnPay;
        private ComponentFactory.Krypton.Toolkit.KryptonGroup flowLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSizeL;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnSizeM;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private ComponentFactory.Krypton.Toolkit.KryptonGroup kryptonGroup1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnHandTossed;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCrustHotDog;
        private System.Windows.Forms.Label label9;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnCrustCheese;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnThinCrust;
        private ComponentFactory.Krypton.Toolkit.KryptonGroup kryptonGroup3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnTPaneer;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnTTomato;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btnTBBQ;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}